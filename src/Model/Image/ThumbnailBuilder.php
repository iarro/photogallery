<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Image;

use Iarro\Photogallery\Model\Filesystem\FsType;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class ThumbnailBuilder
{
    /**
     * @param array{0:int, 1:int} $resolution
     */
    public function __construct(
        #[Autowire('%thumbnail_resolution%')] protected array $resolution,
    ) {
    }

    public function create(FsType $path): \Imagick
    {
        [$expWidth, $expHeight] = $this->resolution;

        $imagick = new \Imagick($path->getRealPath());

        $this->resize($imagick, $expWidth, $expHeight);

        return $imagick;
    }

    protected function resize(\Imagick $imagick, int $expWidth, int $expHeight): void
    {
        $expectedRatio = $expWidth / $expHeight;

        $imagick->autoOrient();

        $width = $imagick->getImageWidth();
        $height = $imagick->getImageHeight();

        [$newWidth, $newHeight] = Calculator::cropTo($width, $height, $expectedRatio);
        [$x, $y] = Calculator::compose($width, $height, $newWidth, $newHeight);

        $imagick->cropImage($newWidth, $newHeight, $x, $y);
        $imagick->resizeImage($expWidth, $expHeight, \Imagick::FILTER_LANCZOS, 1);
    }
}
