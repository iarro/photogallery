<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Image;

class Calculator
{
    /**
     * @return array{0:int, 1:int}
     */
    public static function cropTo(int $width, int $height, float $ratio): array
    {
        $actualRatio = $width / $height;

        if ($ratio > $actualRatio) {
            return [$width, (int) round($width / $ratio)];
        }

        return [(int) round($height * $ratio), $height];
    }

    /**
     * @return array{0:int, 1:int}
     */
    public static function compose(int $width, int $height, int $cropWidth, int $cropHeight): array
    {
        // if the image is significantly taller than wider, the crop is made more towards the Golden section
        $goldenSection = $height / $width >= 1.3;

        if ($goldenSection) {
            $a = 3;
            $b = 2;
        } else {
            $a = 2;
            $b = 2;
        }

        if ($width > $height) {
            $third = $width / $a;
            $half = $cropWidth / $b;

            return [(int) round($third - $half), 0];
        }

        $third = $height / $a;
        $half = $cropHeight / $b;

        return [0, (int) round($third - $half)];
    }
}
