<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Image;

use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Media\Video;
use Iarro\Photogallery\Model\Filesystem\File;
use Iarro\Photogallery\Model\Filesystem\FileFactory;

class ScreenshotBuilder
{
    public function __construct(
        protected ThumbnailBuilder $thumbnailBuilder,
        protected FileFactory $fileFactory,
    ) {
    }

    public function create(File $file): \Imagick
    {
        $filename = tempnam(sys_get_temp_dir(), 'screenshot.jpg');
        if (false === $filename) {
            throw new \RuntimeException('Unable to create temporary screenshot file');
        }

        $ffmpeg = FFMpeg::create();
        $video = $ffmpeg->open($file->getRealPath());

        if (!$video instanceof Video) {
            throw new \InvalidArgumentException(sprintf('File %s is not valid video.', $file->getRealPath()));
        }

        $duration = $video->getFFProbe()->format($file->getRealPath())->get('duration');

        // creates screen from video at 30% of length
        $video
            ->frame(TimeCode::fromSeconds($duration * 0.3))
            ->save($filename);

        $imagick = $this->thumbnailBuilder->create(
            $this->fileFactory->create($filename, true),
        );

        unlink($filename);

        return $imagick;
    }
}
