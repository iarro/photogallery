<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

interface FsTypeFactory
{
    public function create(string $path, bool $isAbsolute = false): FsType;
}
