<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Path;

class FileFactory implements FsTypeFactory
{
    public function __construct(
        #[Autowire('%storage_dir%')] protected string $storageDir,
    ) {
    }

    public function create(string $path, bool $isAbsolute = false): File
    {
        if ($isAbsolute) {
            $path = Path::makeRelative($path, $this->storageDir);
        }

        return new File($path, $this->storageDir);
    }
}
