<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Path;

class DirectoryFactory implements FsTypeFactory
{
    public function __construct(
        #[Autowire('%storage_dir%')] protected string $storageDir,
        protected FileFactory $fileFactory,
    ) {
    }

    public function create(string $path, bool $isAbsolute = false): Directory
    {
        if ($isAbsolute) {
            $path = Path::makeRelative($path, $this->storageDir);
        }

        return new Directory($path, $this->storageDir, $this, $this->fileFactory);
    }
}
