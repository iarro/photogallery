<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

use Symfony\Component\Filesystem\Path;

class File implements FsType
{
    private string|false $mimetype = false;

    public function __construct(
        protected string $relativePath,
        protected string $basePath,
    ) {
    }

    public function getFilename(): string
    {
        return pathinfo($this->relativePath, \PATHINFO_BASENAME);
    }

    public function getRealPath(): string
    {
        return Path::makeAbsolute($this->relativePath, $this->basePath);
    }

    public function getRelativePath(): string
    {
        return $this->relativePath;
    }

    public function getMimeType(): string
    {
        if (false === $this->mimetype) {
            $this->mimetype = mime_content_type($this->getRealPath()) ?: '';
        }

        return $this->mimetype;
    }

    public function isImage(): bool
    {
        return str_starts_with($this->getMimeType(), 'image/');
    }

    public function isVideo(): bool
    {
        return str_starts_with($this->getMimeType(), 'video/');
    }
}
