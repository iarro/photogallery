<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

interface FsType
{
    public function getFilename(): string;

    public function getRealPath(): string;

    public function getRelativePath(): string;
}
