<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Filesystem;

use Symfony\Component\Filesystem\Path;

/**
 * @implements \IteratorAggregate<FsType>
 */
class Directory implements FsType, \IteratorAggregate
{
    public function __construct(
        protected string $relativePath,
        protected string $basePath,
        protected DirectoryFactory $dirFactory,
        protected FileFactory $fileFactory,
    ) {
    }

    public function getFilename(): string
    {
        if ('..' === $this->relativePath) {
            return $this->relativePath;
        }

        return pathinfo($this->relativePath, \PATHINFO_BASENAME);
    }

    public function getRealPath(): string
    {
        return Path::makeAbsolute($this->relativePath, $this->basePath);
    }

    public function getRelativePath(): string
    {
        return $this->relativePath;
    }

    /**
     * @return \Traversable<FsType>
     */
    public function getIterator(): \Traversable
    {
        /** @var iterable<\SplFileInfo> $iterator */
        $iterator = new \RecursiveDirectoryIterator($this->getRealPath(), \RecursiveDirectoryIterator::CURRENT_AS_FILEINFO|\RecursiveDirectoryIterator::SKIP_DOTS);

        /** @var \ArrayIterator<int, FsType> $list */
        $list = new \ArrayIterator();

        foreach ($iterator as $item) {
            if (!$item->isReadable()) {
                continue;
            }

            if ($item->isDir()) {
                $list[] = $this->dirFactory->create($item->getRealPath(), true);
            }

            if ($item->isFile()) {
                $file = $this->fileFactory->create($item->getRealPath(), true);
                if ($file->isImage() || $file->isVideo()) {
                    $list[] = $file;
                }
            }
        }

        $list->uasort($this->sort(...));

        return $list;
    }

    private function sort(FsType $a, FsType $b): int
    {
        if ('..' === $a->getFilename()) {
            return -1;
        }

        if ('..' === $b->getFilename()) {
            return +1;
        }

        if ($a::class === $b::class) {
            return strcasecmp($a->getFilename(), $b->getFilename());
        }

        if ($a instanceof self) {
            return -1;
        } else {
            return +1;
        }
    }
}
