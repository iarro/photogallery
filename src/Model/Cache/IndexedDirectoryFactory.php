<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Cache;

use Iarro\Photogallery\Model\Filesystem\DirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\FileFactory;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Path;

class IndexedDirectoryFactory extends DirectoryFactory
{
    public function __construct(
        #[Autowire('%storage_dir%')] string $storageDir,
        protected FileFactory $fileFactory,
        protected Index $index,
    ) {
        parent::__construct($storageDir, $this->fileFactory);
    }

    public function create(string $path, bool $isAbsolute = false): IndexedDirectory
    {
        if ($isAbsolute) {
            $path = Path::makeRelative($path, $this->storageDir);
        }

        return new IndexedDirectory($path, $this->storageDir, $this, $this->fileFactory, $this->index);
    }
}
