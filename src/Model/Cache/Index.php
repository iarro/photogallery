<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Cache;

use Iarro\Photogallery\Command\IndexerCommand;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * @implements \ArrayAccess<string, array<int, string[]>>
 */
class Index implements \ArrayAccess
{
    /**
     * @var array<string, array<int, string[]>>
     */
    private array|false $index = false;

    public const DIRECTORIES = 0;
    public const FILES = 1;

    public function __construct(#[Autowire('%index_file%')] protected string $file)
    {
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->index[$offset]);
    }

    /**
     * @return array<int, string[]>
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->index[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        throw new \BadMethodCallException(self::class.' is immutable.');
    }

    public function offsetUnset(mixed $offset): void
    {
        throw new \BadMethodCallException(self::class.' is immutable.');
    }

    private function lazyLoad():void
    {
        if (false !== $this->index) {
            return;
        }

        if (!file_exists($this->file) || !is_readable($this->file)) {
            throw new \LogicException('Unable to access index file. Run '.IndexerCommand::getDefaultName().' command.');
        }

        $this->index = include $this->file;
    }
}
