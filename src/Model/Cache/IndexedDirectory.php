<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Model\Cache;

use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Model\Filesystem\DirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\File;
use Iarro\Photogallery\Model\Filesystem\FileFactory;

class IndexedDirectory extends Directory
{
    public function __construct(
        string $relativePath,
        string $basePath,
        DirectoryFactory $dirFactory,
        FileFactory $fileFactory,
        protected Index $index,
    ) {
        parent::__construct($relativePath, $basePath, $dirFactory, $fileFactory);
    }

    public function getIterator(): \Traversable
    {
        $path = $this->getRelativePath();

        if (isset($this->index[$path])) {
            /** @var \ArrayIterator<int, File|Directory> $iterator */
            $iterator = new \ArrayIterator();

            if (isset($this->index[$path][Index::DIRECTORIES])) {
                foreach ($this->index[$path][Index::DIRECTORIES] as $p) {
                    $iterator[] = $this->dirFactory->create($p);
                }
            }

            if (isset($this->index[$path][Index::FILES])) {
                foreach ($this->index[$path][Index::FILES] as $p) {
                    $iterator[] = $this->fileFactory->create($p);
                }
            }

            return $iterator;
        }

        return parent::getIterator();
    }
}
