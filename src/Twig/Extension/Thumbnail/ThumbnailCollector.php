<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Twig\Extension\Thumbnail;

use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Model\Filesystem\File;
use Iarro\Photogallery\Model\Filesystem\FsType;

class ThumbnailCollector
{
    private const IMAGES_IN_THUMBNAIL = 4;

    /**
     * @var File[]
     */
    private array $collectedThumbnails = [];

    private int $count = 0;

    /**
     * @return FsType[]
     */
    public function __invoke(Directory $dir, int $recursionMaxLevel = 4): array
    {
        $this->iterate($dir, $recursionMaxLevel, 0);

        return $this->collectedThumbnails;
    }

    private function iterate(Directory $dir, int $maxLevel, int $level): void
    {
        if ($maxLevel < ++$level) {
            return;
        }

        foreach ($dir as $item) {
            if ($item instanceof File && $item->isImage()) {
                $this->collectedThumbnails[] = $item;
                ++$this->count;

                if (self::IMAGES_IN_THUMBNAIL <= $this->count) {
                    return;
                }
            }
        }

        foreach ($dir as $item) {
            if ($item instanceof Directory) {
                $this->iterate($item, $maxLevel, $level);

                if (self::IMAGES_IN_THUMBNAIL <= $this->count) {
                    return;
                }
            }
        }
    }
}
