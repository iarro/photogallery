<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Twig\Extension;

use Iarro\Photogallery\Model\Cache\IndexedDirectory;
use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Twig\Extension\Thumbnail\ThumbnailCollector;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ThumbnailExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_thumbnail_images', $this->getThumbnailImages(...)),
        ];
    }

    private function getThumbnailImages(Directory $object): array
    {
        if ($object instanceof IndexedDirectory) {
            return (new ThumbnailCollector())($object);
        }

        return (new ThumbnailCollector())($object, 1);
    }
}
