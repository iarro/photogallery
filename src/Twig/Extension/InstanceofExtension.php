<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Twig\Extension;

use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Model\Filesystem\File;
use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class InstanceofExtension extends AbstractExtension
{
    public function getTests(): array
    {
        return [
            new TwigTest('directory', fn (mixed $object): bool => $object instanceof Directory),
            new TwigTest('image', fn (mixed $object): bool => $object instanceof File && $object->isImage()),
            new TwigTest('video', fn (mixed $object): bool => $object instanceof File && $object->isVideo()),
        ];
    }
}
