<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Command;

use Iarro\Photogallery\Model\Cache\Index;
use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Model\Filesystem\DirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;

class IndexerCommand extends Command
{
    protected string $rootDir;

    public function __construct(
        protected DirectoryFactory $directoryFactory,
        #[Autowire('%index_file%')] protected string $indexFile,
        #[Autowire('%storage_dir%')] protected string $baseDir,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): ?string
    {
        return 'photogallery:index';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $rootDir = $this->directoryFactory->create($this->baseDir, true);

        $list = [];
        $this->collect($rootDir, $list);

        $content = var_export($list, true);
        $filesystem = new Filesystem();
        $filesystem->dumpFile($this->indexFile, '<?php return '.$content.';');

        return Command::SUCCESS;
    }

    /**
     * @param array<string, array<int, string>> $list
     */
    private function collect(Directory $directory, array &$list): bool
    {
        $hasImages = false;

        foreach ($directory as $item) {
            if ($item instanceof File) {
                $list[$directory->getRelativePath()][Index::FILES][] = $item->getRelativePath();
                $hasImages = true;
            }

            if ($item instanceof Directory) {
                if ($this->collect($item, $list)) {
                    $list[$directory->getRelativePath()][Index::DIRECTORIES][] = $item->getRelativePath();
                    $hasImages = true;
                }
            }
        }

        return $hasImages;
    }
}
