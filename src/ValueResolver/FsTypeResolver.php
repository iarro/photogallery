<?php

declare(strict_types=1);

namespace Iarro\Photogallery\ValueResolver;

use Iarro\Photogallery\Model\Cache\IndexedDirectory;
use Iarro\Photogallery\Model\Cache\IndexedDirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\Directory;
use Iarro\Photogallery\Model\Filesystem\DirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\File;
use Iarro\Photogallery\Model\Filesystem\FileFactory;
use Iarro\Photogallery\Model\Filesystem\FsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsTargetedValueResolver;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

#[AsTargetedValueResolver]
class FsTypeResolver implements ValueResolverInterface
{
    public function __construct(
        protected readonly FileFactory $fileFactory,
        protected readonly DirectoryFactory $directoryFactory,
        protected readonly IndexedDirectoryFactory $indexedDirectoryFactory,
    ) {
    }

    /** @return array<FsType|null> */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (null === $argument->getType()) {
            return [];
        }

        if (!is_subclass_of($argument->getType(), FsType::class)) {
            return [];
        }

        $value = $request->attributes->getString($argument->getName());

        switch ($argument->getType()) {
            case File::class:
                return [$this->fileFactory->create($value)];
            case IndexedDirectory::class:
                return [$this->indexedDirectoryFactory->create($value)];
            case Directory::class:
                return [$this->directoryFactory->create($value)];
        }

        return [];
    }
}
