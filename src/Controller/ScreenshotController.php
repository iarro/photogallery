<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Controller;

use Iarro\Photogallery\Model\Cache\IndexedDirectoryFactory;
use Iarro\Photogallery\Model\Filesystem\File;
use Iarro\Photogallery\Model\Filesystem\FileFactory;
use Iarro\Photogallery\Model\Image\ScreenshotBuilder;
use Iarro\Photogallery\ValueResolver\FsTypeResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\HttpKernel\Attribute\ValueResolver;
use Symfony\Component\Routing\Attribute\Route;

#[Cache(maxage: 2628000, public: true, mustRevalidate: false)]
#[Route('/screenshot/{video}', name: 'screenshot', requirements: ['video' => '.*'])]
class ScreenshotController extends AbstractController
{
    public function __construct(
        protected ScreenshotBuilder $builder,
        protected IndexedDirectoryFactory $directoryFactory,
        protected FileFactory $fileFactory,
        #[Autowire('%storage_dir%')] protected string $storageDir,
    ) {
    }

    public function __invoke(#[ValueResolver(FsTypeResolver::class)] File $video): Response
    {
        $imagick = $this->builder->create($video);

        return new Response(
            $imagick->getImageBlob(),
            headers: ['Content-Type' => $imagick->getImageMimeType()],
        );
    }
}
