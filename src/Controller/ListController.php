<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Controller;

use Iarro\Photogallery\Model\Cache\IndexedDirectory;
use Iarro\Photogallery\ValueResolver\FsTypeResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\ValueResolver;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/{directory}', name: 'list', requirements: ['directory' => '.*'], priority: -100000)]
class ListController extends AbstractController
{
    public function __construct(
        #[Autowire('%storage_dir%')] protected string $storageDir,
    ) {
    }

    public function __invoke(#[ValueResolver(FsTypeResolver::class)] IndexedDirectory $directory): Response
    {
        // find parent directory
        $parent = Path::makeRelative($directory->getRealPath().'/../', $this->storageDir);
        $parent = ('..' !== $parent) ? $parent : null;

        return $this->render('list/index.html.twig', [
            'directory' => $directory,
            'parent' => $parent,
        ]);
    }
}
