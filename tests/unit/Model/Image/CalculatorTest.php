<?php

declare(strict_types=1);

namespace Iarro\Photogallery\Tests\unit\Model\Image;

use Iarro\Photogallery\Model\Image\Calculator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    #[DataProvider('cropProvider')]
    public function testCropTo(int $width, int $height, float $ratio, int $expectedWidth, int $expectedHeight): void
    {
        [$actualWidth, $actualHeight] = Calculator::cropTo($width, $height, $ratio);

        self::assertEquals($expectedWidth, $actualWidth);
        self::assertEquals($expectedHeight, $actualHeight);
    }

    /**
     * @return array{array<int|float>}
     */
    public static function cropProvider(): array
    {
        // original width, original height, aspect ratio, expected width, expected height
        return [
            [320, 240, 4/3, 320, 240],
            [400, 240, 4/3, 320, 240],
            [400, 270, 4/3, 360, 270],
            [360, 300, 4/3, 360, 270],
        ];
    }
}
