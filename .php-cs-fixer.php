<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/public')
    ->in(__DIR__.'/src')
    ->in(__DIR__.'/tests')
    ->name('*.php');

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'phpdoc_to_comment' => false,
        'phpdoc_separation' => false,
        'single_line_throw' => false,
        'binary_operator_spaces' => false,
        'trailing_comma_in_multiline' => true,
        'ordered_imports' => ['imports_order' => ['class', 'function', 'const'], 'sort_algorithm' => 'alpha'],
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
