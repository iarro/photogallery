FROM php:8.2-fpm-alpine

WORKDIR /var/www/html

RUN apk update

RUN apk --no-cache add nginx supervisor

RUN apk --no-cache add pcre-dev ${PHPIZE_DEPS} \
    imagemagick imagemagick-dev imagemagick-libs && \
    pecl install imagick && \
    docker-php-ext-enable imagick && \
    apk del pcre-dev ${PHPIZE_DEPS}

RUN apk --no-cache add pcre-dev ${PHPIZE_DEPS} \
    linux-headers && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    apk del pcre-dev ${PHPIZE_DEPS}

RUN apk --no-cache add libzip-dev zip && \
    docker-php-ext-install zip

RUN apk --no-cache add icu-dev && \
    docker-php-ext-install -j$(nproc) intl

RUN apk --no-cache add ffmpeg

RUN docker-php-ext-install -j$(nproc) ctype
RUN docker-php-ext-install -j$(nproc) exif

COPY .docker/conf/nginx/nginx.conf    /etc/nginx/nginx.conf
COPY .docker/conf/nginx/default.conf  /etc/nginx/conf.d/default.conf

COPY .docker/conf/php/php.ini      /usr/local/etc/php/conf.d/php.ini
COPY .docker/conf/php/fpm-pool.ini /usr/local/etc/php/conf.d/fpm-pool.ini
COPY .docker/conf/php/xdebug.ini   /usr/local/etc/php/conf.d/docker-php-ext-01-xdebug.ini

COPY .docker/conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN chown -R www-data:www-data /var/www/html /run /var/lib/nginx /var/log/nginx
USER www-data
COPY --chown=www-data . /var/www/html/

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_PROCESS_TIMEOUT=1200

EXPOSE 8080

# set cron job
#RUN mkdir /etc/cron && \
#    echo "* */4 * * * /var/www/html/bin/console photogallery:index" > /etc/cron/crontab && \
#    crontab /etc/cron/crontab

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
